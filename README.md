

## Generic images for use on process-container runtimes.

Instructions for build with Fusion on MacOS. I don't use docker4mack since it's 
really broken.

```
❯ vctl build --file=generic-focal-ubuntu.Dockerfile --tag generic-focal-ubuntu .
❯ vctl tag generic-focal-ubuntu registry.gitlab.com/versionsix/bigfish/generic-focal-ubuntu
❯ vctl push registry.gitlab.com/versionsix/bigfish/generic-focal-ubuntu

```
