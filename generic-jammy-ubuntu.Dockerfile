FROM ubuntu:22.04 as release

# Unattended
ENV DEBIAN_FRONTEND noninteractive

ADD https://github.com/sharkdp/bat/releases/download/v0.22.1/bat_0.22.1_amd64.deb /tmp/bat.deb
ADD https://github.com/Peltoche/lsd/releases/download/0.23.1/lsd_0.23.1_amd64.deb /tmp/lsd.deb
RUN echo "**** install runtime packages ****" && \
  apt -y \
    -o Dpkg::Progress-Fancy="0" \
    -o Debug::Acquire::http=false \
    -o Acquire::http::Timeout="10" \
    -o Acquire::ftp::Timeout="10" \
      update && \
  apt -y \
    -o Dpkg::Progress-Fancy="0" \
    -o Debug::Acquire::http=false \
    -o Acquire::http::Timeout="10" \
    -o Acquire::ftp::Timeout="10" \
    -o Dpkg::Options::="--force-confdef" \
    -o Dpkg::Options::="--force-confold" \
    --no-install-recommends \
      install \
        bind9-dnsutils \
        coreutils \
        curl \
        dnsutils \
        hwloc-nox \
        iperf3 \
        iproute2 \
        iputils-arping \
        iputils-ping \
        jq \
        less \
        mini-httpd \
        mtr-tiny \
        nano \
        ncdu \
        netcat \
        procps \
        psmisc \
        screen \
        setserial \
        tcpdump \
        telnet \
        tmux \
        traceroute \
        wget \
          && \
    dpkg -i /tmp/bat.deb && \
    dpkg -i /tmp/lsd.deb && \
    apt -y autoremove && \
    rm -Rf /var/lib/apt/lists/* && \
    rm -Rf /usr/share/{doc,man} && \
    rm -Rf /tmp && \
    apt clean

ENV HOME="/root" \
LANGUAGE="en_US.UTF-8" \
LANG="en_US.UTF-8" \
TERM="xterm"

# User shell.
RUN printf '[ -z "$PS1" ] && return\n\
HISTCONTROL=ignoredups:ignorespace\n\
shopt -s histappend\n\
HISTSIZE=1000\n\
HISTFILESIZE=2000\n\
IGNOREEOF=2\n\
\n\
\n'\
>> /root/.bashrc

# Prevent accidental exit.
ENV IGNOREEOF 2
WORKDIR /root
CMD ["bash", "--init-file", "/root/.bashrc"]

# Tests
FROM release

RUN ip -c a
RUN curl --version

# Final
FROM release
